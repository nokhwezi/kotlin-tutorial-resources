fun main(args: Array<String>) {
    val byte: Byte = -12  //Byte can hold 8 bits (256 number combinations) -128 to 127
    println(byte)

    val short: Short = 15000 // Short can hold 16 bits. 2 to the power 16 for the max number it can hold https://kotlinlang.org/docs/basic-types.html#numbers
    println(short)

    val int: Int = 500000
    println(int)

    val long: Long = 1000000
    println(long)

    val float: Float = 5890.08f
    println(float)

    val double: Double = 83928.23
    println(double)
}