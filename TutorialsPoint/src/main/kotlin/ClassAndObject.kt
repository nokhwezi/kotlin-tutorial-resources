class ClassAndObject {
    // property (data member)
    private var name: String = "Tutorials Point"

    // member function
    fun printMe() {
        print("You are at the best learning website named $name")
    }
}
fun main(args: Array<String>) {
    val obj = ClassAndObject() // create 'obj' object of ClassAndObject class
    obj.printMe()

}