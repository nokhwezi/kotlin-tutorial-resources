//Private
private class VisibiltyControl {
    private val i = 1 //accessible within its immediate scope
    private fun doSomething() {}
}

//Protected
open class X() {
    protected val i = 1 //visible to its subclass only.
}
class Y : X() {
    fun getValue() : Int {
        return i
    }
}

//Internal
class internalExample {
    internal val i = 1 //visible only inside the module under which it is implemented
    internal fun doSomething() {}
}

//Public
class publicExample {
    val i = 1 //accessible from anywhere in the project workspace
    fun doSomething() {
    }
}