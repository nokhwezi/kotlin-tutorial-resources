fun main(args: Array<String>) {
    val demo = Outer().Nested().foo() // calling nested class method
    print(demo)
}
class Outer {
    private val welcomeMessage: String = "Welcome to the TutorialsPoint.com"
    inner class Nested { //An inner class can be accessed by the data member of the outer class.
        fun foo() = welcomeMessage
    }
}

