interface Interface {
    var myVar: Int            // abstract property
    fun absMethod():String    // abstract method

    fun hello() {
        println("Hello there, Welcome to TutorialsPoint.Com!")
    }
}
class InterfaceImp : Interface {
    override var myVar: Int = 25
    override fun absMethod() = "Happy Learning "
}

interface A {
    fun printMe() {
        println("\nMethod of interface A")
    }
}
interface B  {
    fun printMeToo() {
        println("I am another Method from interface B")
    }
}

// implements two interfaces A and B
class MultipleInterfaceExample: A, B

fun main(args: Array<String>) {
    val obj = InterfaceImp()
    println("My Variable Value is = ${obj.myVar}")
    print("Calling hello(): ")
    obj.hello()

    print("Message from the Website-- ")
    println(obj.absMethod())
//mutliple interfaces
    val ob = MultipleInterfaceExample()
    ob.printMe()
    ob.printMeToo()
}