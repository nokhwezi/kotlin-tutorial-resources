class Extention {
    var skills : String = "null"

    fun printMySkills() {
        println(skills)
    }
}
fun main(args: Array<String>){
    var  a1 = Extention()
    a1.skills = "JAVA"
    //a1.printMySkills()
    var  a2 = Extention()
    a2.skills = "SQL"
    //a2.printMySkills()

    var  a3 = Extention()
    a3.skills = a1.addMySkills(a2)
    a3.printMySkills()

    //
    println("Heyyy!!!"+C.show())
}

//Function Extension
fun Extention.addMySkills(a:Extention):String{ //define a method outside of the main class
    var a4 = Extention()
    a4.skills = this.skills + " " +a.skills
    return a4.skills
}

//Object Extention
class C {
    companion object {
        fun show():String {
            return("You are learning Kotlin")
        }
    }
}