fun main(args: Array<String>) {
    var objet = GenericsExample<String>("JAVA")
    var objet1 = GenericsExample<Int>(10)
    println(objet)
    println(objet1)
}
class GenericsExample<T>(input:T) { //generics is defined by <T> where “T” stands for template. Best used when we dont know the data type that will be used
    init {
        println("I am getting called with the value $input")
    }
}