sealed class SealedClass { //This type of class is used to represent a restricted class hierarchy
    class OP1 : SealedClass() // SealedClass class can be of two types only
    class OP2 : SealedClass()
}

fun main(args: Array<String>) {
    val obj: SealedClass = SealedClass.OP2()

    val output = when (obj) { // defining the object of the class depending on the inputs
        is SealedClass.OP1 -> "Option One has been chosen"
        is SealedClass.OP2 -> "option Two has been chosen"
    }

    println(output)
}
