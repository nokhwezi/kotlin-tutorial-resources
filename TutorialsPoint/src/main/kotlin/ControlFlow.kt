fun main(args: Array<String>){
    //If-Else
    val a = 5
    val b = 7
    var max: Int

    if(a < b)
        max = b
    else
        max = a
    print("Maximum number is $max")

    //Use of WHEN
    val x:Int = 5
    when (x) {
        1 -> print("x == 1") // or write it like 1,2 -> print("value of x is either 1 or 2)
        2 -> print("x == 2")

        else -> { // Note the block
            print("\nx is neither 1 nor 2\n")
        }
    }

    //FOR Loop
    val items = listOf(1, 2, 3, 4)
    for (i in items)
        println("values of the array$i")
    print("\n\n")

    for ((index,value ) in items.withIndex()){
        println("The element at $index is $value")
    }

    //WHILE LOOP
    var y:Int = 0
    println("\nExample of While Loop--")

    while(y <= 10) {
        print(y)
        y++
    }
    print("\n")

    //DO-WHILE LOOP
    var z:Int = 0
    print("\n")
    do {
        z += 10
        println("I am inside Do block---$z")
    } while(z <= 50)

    //Use of RETURN
    var value:Int = 10
    println("\nThe value is--" + doubleMe(value))

    //Use of Continue&Break
    println("\nExample of Break and Continue")
    myLabel@ for(d in 1..10) { // appling the custom label
        if(d == 5) {
            println("I am inside if block with value"+x+"\n-- hence it will close the operation")
            break@myLabel //specifing the label
        } else {
            println("I am inside else block with value"+x)
            continue@myLabel
        }
    }

}

fun doubleMe(value:Int):Int {
    return 2*value
}