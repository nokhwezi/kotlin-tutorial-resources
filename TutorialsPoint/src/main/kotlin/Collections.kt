fun main(args: Array<String>){
    val digits: MutableList<Int> = mutableListOf(1,2,3) //a mutable list of numbers
    val readOnlyView: List<Int> = digits  //immutable list
    println("Mutable list is $digits")

    digits.add(4)

    println("Mutable list AFTER adding value $digits")
    println(readOnlyView)
   // readOnlyView.clear()  //does not compile coz its immutable

    //NEXT SECTION for Map and Set
    val numbers = listOf(1,2,3,4)
    println("\nFirst element is ${numbers.first()}")
    println("Last element is ${numbers.last()}")
    println("Even numbers is ${numbers.filter { it % 2 == 0 }}") //'it' is the value parameter for the list

    val readWriteMap = hashMapOf("foo" to 1, "bar" to 2)
    println(readWriteMap["foo"]) //prints 1

    val strings = hashSetOf("a", "b","b", "c", "c")
    println("Set values are $strings")
}