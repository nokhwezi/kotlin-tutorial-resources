import kotlin.properties.Delegates
import kotlin.reflect.KProperty

//Delegation means passing the responsibility to another class or metho
//Delegation Design Pattern
interface Base {
    fun printMe() //abstract method
}

class BaseImpl(val x: Int) : Base {
    override fun printMe() { println(x) }   //implementation of the method
}
//we are using this implementation using “by” keyword.
class Derived(b: Base) : Base by b  // delegating the public method on the object b

/*
        Using Lazy()
Lazy is a lambda function which takes a property as an
input and in return gives an instance of Lazy<T>,
where <T> is basically the type of the properties it is using.
*/
val myVar: String by lazy {
    "Hello"
}

//Delegation.Observable
class User {
    var name: String by Delegates.observable("Welcome to Tutorialspoint.com") {
            prop, old, new ->
        println("$old -> $new")
    }
}
/*The get() and set() methods of the variable p will be delegated to its
getValue() and setValue() methods defined in the Delegate class.
 */

class Example {
    var p: String by Delegate()
}

class Delegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "$thisRef, thank you for delegating '${property.name}' to me!"
    }
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("$value has been assigned to '${property.name} in $thisRef.'")
    }
}
//End of example

fun main(args: Array<String>) {
    //delegation design pattern
    val b = BaseImpl(10)
    Derived(b).printMe() // prints 10 :: accessing the printMe() method
    //Lazy()
    println("\n$myVar My dear friend\n")
    //Observable
    val user = User()
    user.name = "first"
    user.name = "second"
}

