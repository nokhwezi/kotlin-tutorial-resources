fun main(args: Array<String>) {
    val person1 = Constructors("Khwezi", 15)
    println(person1.message)
    println("First Name = ${person1.firstName}")
    println("Age = ${person1.age}")
}

class Constructors (val firstName: String, var age: Int){
    val message: String = "Personel Deets"
    constructor(name : String , age :Int ,message :String):
            this(name,age) {
    }
}