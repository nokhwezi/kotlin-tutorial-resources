fun main(args: Array<String>) {
    println(myFunction("The Good Life"))
    //lambda function
    val mylambda :(String)->Unit  = {
            s:String->println(s)
    }
    val v:String = "Lambda print out"
    mylambda(v)
    ////passing lambda as a parameter of another function
    myFun(v,mylambda)
}
fun myFunction(x: String): String {
    var c: String = "Hey!! Welcome To "
    return ("$c$x\n")
}

fun myFun(a :String, action: (String)->Unit) { //passing lambda
    print("This is an inline function using ")
    action(a)// call to lambda function
}