fun main(args: Array<String>) {
    val book: Book = Book("Kotlin", "Nokhwezi", 7)
    println("Name of the Book is " +book.name)
    println("Puclisher Name " +book.publisher)
    println("Review of the book is " +book.reviewScore)
    book.reviewScore = 9
    println("Printing all the info all together -> ${book.toString()}")
    //using inbuilt function of the data class

    println("Example of the hashCode function: ${book.hashCode()}")
}

//need to have one primary constructor and all the primary constructor should have at least one parameter.
//can use some of the inbuilt function of that data class such as “toString()”,”hashCode()”,etc
data class Book(val name: String, val publisher: String, var reviewScore: Int)