open class Inheritance { // open keyword to not make it default final
    open fun think () {
        print("Hey!! i am thiking ")
    }
}
class BCD: Inheritance(){ // inheritence happend using default constructor
    override fun think(){
        print("I am from child")
    }


}

fun main(args: Array<String>) {
    var  a = BCD()
    a.think()
}
